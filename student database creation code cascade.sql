create schema game;
use game;
CREATE TABLE gAdmin(
adminID int NOT NULL auto_increment,
name varchar(160),
school varchar(60),
PRIMARY KEY(adminID) );

CREATE TABLE gTeacher(
teachID int NOT NUll auto_increment,
username varchar(160),
userpass varchar(160),
PRIMARY KEY (teachID)
);

CREATE TABLE gStudent(
studID int NOT NULL auto_increment,
username varchar(160),
teach int NOT NULL,
PRIMARY KEY (studID),
foreign key (teach) REFERENCES gTeacher(teachID)
       ON DELETE CASCADE
       ON UPDATE CASCADE
);

CREATE TABLE gCategory(
catID   int NOT NULL auto_increment,
name varchar(160),
PRIMARY KEY (catID)
);
CREATE TABLE gModule(
modID   int NOT NULL auto_increment,
score   decimal,
mName   varchar(60),
catID   int,
sConstraint int,
studID  int NOT NULL,
PRIMARY KEY (modID),
FOREIGN KEY (catID) REFERENCES gCategory(catID)      
	   ON DELETE CASCADE
       ON UPDATE CASCADE,
FOREIGN KEY (studID) REFERENCES gStudent(studID)
       ON DELETE CASCADE
       ON UPDATE CASCADE

);
CREATE TABLE gScore(
scoreID int NOT NULL auto_increment,
teach   int NOT NULL ,
stud    int NOT NULL,
sCat    int NOT NULL,
PRIMARY KEY (scoreID),
foreign key (teach) REFERENCES gTeacher(teachID)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
foreign key (stud) REFERENCES gStudent(studID)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
foreign key (sCat) REFERENCES gCategory(catID)
       ON DELETE CASCADE
       ON UPDATE CASCADE
);

CREATE TABLE gQuestion(
questionID int NOT NULL auto_increment,
questBody varchar(1200),
teachID int,
modID int,
PRIMARY KEY (questionID),
foreign key (teachID) REFERENCES gteacher(teachID)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
foreign key (modID) REFERENCES gModule(modID)
       ON DELETE CASCADE
       ON UPDATE CASCADE
);

CREATE TABLE gAnswer(
answerID int NOT NULL auto_increment,
answerBody varchar(1200),
isCorrect bit,
modID int,
questionID int,
PRIMARY KEY (answerID),
FOREIGN KEY (modID) REFERENCES gModule(modID)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
FOREIGN KEY (questionID) REFERENCES gQuestion(questionID)
       ON DELETE CASCADE
       ON UPDATE CASCADE
);