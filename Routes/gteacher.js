var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var saltRounds = 10;
var auth = require('../controllers/auth');
var passportService = require('../services/passport');
var passport = require('passport');
var requireAuth = passport.authenticate('jwt', {
    session: false
});
var requireSignin = passport.authenticate('local', {
    session: false
});
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});
// Helper function to add a teacher to the database
// It calls the insert teacher query and creates a hashed password
function addTeacher(res, username, password) { //hash the new password
    bcrypt.hash(password, saltRounds, function (err, hash) {
        pool.query(
            insertTeacher, [username, hash],
            function (error, results, fields) {
                if (error) throw error;
                rInsertID = 'insertID' + ':' + results.insertId;
                res.json({
                    'hash': hash,
                    'insertID': results.insertId,
                    'username': username,
                });
            }
        );
    });
}


//mysql querys for the scores
var insertTeacher = "INSERT INTO `gteacher`(username,userpass) Values(?,?)";
var getteacherById = "SELECT * FROM `gteacher` WHERE teachID =?";
var getTeacherByUsername = "SELECT * FROM `gteacher` WHERE username =?";
var getAllStuds = "SELECT * FROM `gteacher`";
var deleteTeacher = "DELETE FROM `gteacher` WHERE teachID = ?";
var updateTeacher = "UPDATE `gteacher`  SET username = ? WHERE teachID=?";
var getTeacherPass = "SELECT userpass FROM `gteacher` WHERE username = ?";


//define score routes

router.route('/teacher/signup')
    .post(function (req, res) {
        var username = req.body.username;
        var userpass = req.body.userpass;
        if (!username || !userpass) {
            res.status(422).send({
                error: 'You must enter both username and password'
            });
        } else {
            pool.query(
                getTeacherByUsername, [username],
                function (error, results, fields) {
                    if (error) throw error;
                    if (!results[0]) {
                        addTeacher(res, username, userpass);
                    } else {
                        res.status(422).send({
                            error: 'username is already in use'
                        });
                    }
                }
            );
        }
    });

router.route('/teacher/add')
    //add teachers to server
    .post(function (req, res) {
        var teachName = req.body.username;
        var teachPassword = req.body.password;
        addTeacher(res, teachName, teachPassword);
    });
router.route('/teacher/')
    //get all teacher
    .get(function (req, res) {
        pool.query(getAllStuds, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });
    });

router.route('/teacher/:id')
    //get teachers by id
    .get(function (req, res) {
        var userteachID = req.params.id;
        pool.query(getteacherById, userteachID, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });

    });
router.route('/teacher/update/:id')
    //update teacher by id    
    .put(function (req, res) {
        var teachName = req.body.username;
        var userteachID = req.params.id;
        pool.query(
            updateTeacher, [teachName, userteachID],
            function (error, results, fields) {
                if (error) throw error;
                res.json(req.body);
            });
    });

router.route('/teacher/del/:id')
    //delete teacher
    .delete(function (req, res) {
        var userteachID = req.params.id;
        pool.query(deleteTeacher, userteachID, function (error, results, fields) {
            if (error) throw error;
            res.json({
                id: req.params.id,
                isDeleted: 'true'
            });
        });
    });
router.route('/teacher/signin')
    .post(requireSignin,auth.signin);