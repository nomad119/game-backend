var chai = require('chai');
var chaiHttp = require('chai-http');
var gserver = require('../../server');
var should = chai.should();

chai.use(chaiHttp);
var teachID = 0;
describe('Teachers Calls', () => {
    afterEach(() => {
        gserver.close();
    });
    it('gets all teachers', function (done) {
        chai.request(gserver)
            .get('/api/teacher/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                done();
            });
    });
    it('creates a teacher', function (done) {
        chai.request(gserver)
            .post('/api/teacher/add')
            .send({
                "username": "Justin",
                "password": "123"
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('username');
                res.body.username.should.have.equal('Justin');
                teachID = res.body.insertId;
                done();
            });
    });
    it('gets one teacher', function (done) {
        chai.request(gserver)
            .get('/api/teacher/' + teachID)
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                done();
            });
    });
    it('updates a teacher', function (done) {
        chai.request(gserver)
            .put('/api/teacher/update/' + teachID)
            .send({
                "username": "James"
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('username');
                res.body.username.should.have.equal('James');
                done();
            });
    });
    /*    it('deletes a teacher', function(done){
            chai.request(gserver)
            .delete('/api/teacher/del/'+teachID)
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('isDeleted');
                res.body.isDeleted.should.have.equal('true');
                done();
            });
        });
    */
    it('signs a teacher in', function (done) {
        chai.request(gserver)
            .post('/api/teacher/signin')
            .send({
                "username": "Justin",
                "userpass": "123"
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('response');
                res.body.response.should.have.equal(true);
                done();
            });
    });
    it('detects a teacher username already exists on signup', function (done) {
        chai.request(gserver)
            .post('/api/teacher/signup')
            .send({
                "username": "Justin",
                "userpass": "123"
            })
            .end(function (err, res) {
                res.should.have.status(422);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.error.should.have.equal('username is already in use');
                done();
            });
    });
    it('detects a teacher username or password do not exists on signup', function (done) {
        chai.request(gserver)
            .post('/api/teacher/signup')
            .send({
                "username": "Justin"
            })
            .end(function (err, res) {
                res.should.have.status(422);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.error.should.have.equal('You must enter both username and password');
                done();
            });
    });
    it('signs a new teacher up', function (done) {
        chai.request(gserver)
            .post('/api/teacher/signup')
            .send({
                "username": "Lisa",
                "userpass": "123"
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('username');
                res.body.username.should.have.equal('Lisa');
                done();
            });
    });
    it('signs in a teacher', function (done) {
        chai.request(gserver)
            .post('/api/teacher/signin')
            .send({
                "username": "Lisa",
                "userpass": "123"
            })
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('username');
                res.body.username.should.have.equal('Lisa');
                done();
            });
    });
});