var passport = require('passport');
var config = require('../config');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var LocalStrategy = require('passport-local');
var auth = require('../controllers/auth');
var config = require('../config');
var mysql = require('mysql');
var bcrypt = require('bcrypt');
var pool = mysql.createPool({
    host: config.mysqlHost,
    user: config.mysqlUsername,
    password: config.mysqlPassword,
    database: config.mysqlDatabase
});
// Setup options for JWT Strategy
var jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.secret
};
var localOptions = {
    usernameField: 'username',
    passwordField: 'userpass'
};

// Create local strategy
var localLogin = new LocalStrategy(localOptions, function (username, userpass, done) {
    //Verify this email and password, call done with the user
    //if it is the correct email and apssword
    //otherwise, call done with false
    console.log("USERNAME",username);
    var getTeacherPass = "SELECT * FROM `gteacher` WHERE username = ?";
    pool.query(
        getTeacherPass, [username],
        function (error, results, fields) {
            if (error) throw error;
            if (results[0]) {
                var first = results[0];
                var checkHash = results[0].userpass;
                bcrypt.compare(userpass, checkHash, function (err, isMatch) {
                    if (isMatch) {
                        done(null, first.teachID)
                    }
                });
            }
        }
    );
});

// Create JWT strategy
var jwtLogin = new JwtStrategy(jwtOptions, function (payload, done) {
    // See if the user ID in the payload exists in our database
    // if it does, call 'done' with that user
    //otherwise, call done without a user object
    var getteacherById = "SELECT * FROM `gteacher` WHERE teachID =?";
    pool.query(getteacherById, [payload.sub],
        function (error, results, fields) {
            if (error) throw error;
            if (results[0]) {
                var first = results[0]
                console.log(first.teachID);
                done(null, {
                    username: first.username,
                    userpass: first.userpass,
                    id: first.teachID
                })
            }
        }) 
});
// Tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);