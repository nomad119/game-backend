import React, { Component } from "react";
import { connect} from "react-redux"; 
import Students from './Students';
import Classes from './Classes';
import Modules from './Modules';
import Navigation from '../components/Navigation';
import {Route } from 'react-router';

class Home extends Component{
    render(){
        return(
            <div>
        <Navigation />
        <Route path= '/students' component={Students} />
        <Route path= '/classes' component={Classes} />
        <Route path = '/modules' component={Modules} />
            </div>
        );
    }
}
export default connect()(Home);
