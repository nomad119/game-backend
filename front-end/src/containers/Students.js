import React, { Component } from "react";
import { connect} from "react-redux"; 
import * as actions from '../actions';
import Navbar from '../components/Navigation';

class Students extends Component{
    componentDidMount(){
        console.log("STUDENTS UNITE!")
        this.props.fetchStudents();
    }

    render(){
        return(
  
            <div>
            <Navbar />
                Students
            </div>
        );
    }
}
export default connect(null,actions)(Students);