export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = 'auth_error';
export const FETCH_MESSAGE = 'fetch_message';
export const FETCH_STUDENTS = 'fetch_students';
export const FETCH_MODULES = 'fetch_modules';
export const FETCH_CLASSES = 'fetch_classes';