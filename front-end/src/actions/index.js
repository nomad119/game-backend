import axios from 'axios';
import {AUTH_USER, AUTH_ERROR, UNAUTH_USER, FETCH_MESSAGE, FETCH_STUDENTS} from './types';
import history from '../history';
const ROOT_URL = 'http://localhost:8081/api';

export function signupUser({username, userpass}){

    return function(dispatch){
        console.log("signing user up", username);
    axios.post(`${ROOT_URL}/teacher/signup`,{username,userpass})
    .then(response =>{
        
        console.log(response);

        dispatch({type:AUTH_USER});
        localStorage.setItem('token', response.data.token);
        history.push('/dashboard');
    })
    .catch(error =>{
        dispatch(authError(error.response.data.error)
    )
    });
    };
}
export function signinUser({username,userpass}){
    return function(dispatch){
        axios.post(`${ROOT_URL}/teacher/signin`, {username, userpass})
        .then( response =>{
        //If request is good
        //-Update state to show user is authenticated
        // -Save the JWT token
            console.log(response);
            dispatch({ type: AUTH_USER});
            localStorage.setItem('token', response.data.token);
        // -redirect to the route  '/feature'
            fetchStudents();
            history.push('/dashboard');
            return {};
        })
        .catch(() =>{
            dispatch(authError('Bad Login Info'));
        });
    };
}

export function authError(error){
    return{
        type: AUTH_ERROR,
        payload: error
    };
}
export function signoutUser(){
    localStorage.removeItem('token');

    return {type:UNAUTH_USER, authenticated: false}
}

export function fetchMessage(){
    return function(dispatch){
        axios.get(ROOT_URL, {
            headers: {authorization: localStorage.getItem('token')}
        })
            .then(response =>{
                 dispatch({
                    type:FETCH_MESSAGE,
                    payload:response.data.message
                });
            }
        );
    }
}
export function fetchStudents(){
    return function(dispatch){
        axios.get(ROOT_URL, {
            headers: {authorization: localStorage.getItem('token')}
        })
            .then(response =>{
                 dispatch({
                    type:FETCH_STUDENTS,
                    payload:response.data.students
                });
            }
        );
    }
}