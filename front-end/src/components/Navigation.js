import React, { Component } from "react"; 

import { Route, NavLink } from 'react-router-dom';

import Students from "../containers/Students";
import Classes from "../containers/Classes";
import Modules from "../containers/Modules";

class Navigation extends Component{
    constructor(props){
        super(props);
        this.state = {
            navItems: ['Students','Classes','Modules'],
            isOpen:false,
        }
    }
    render(){
        return(
            <div>
             <ul>
                <li> <NavLink to="/students" >Students</NavLink> </li>
                <li> <NavLink to="/classes"  >Classes</NavLink> </li>
                <li> <NavLink to="/modules"  >Modules</NavLink> </li>     
            </ul>   
            <Route path= '/students' component={Students} />

            <Route path="/classes" component={Classes} />

            <Route path="/modules" component={Modules} />
            </div>  
        );
    }
}
export default Navigation;