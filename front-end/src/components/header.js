import React, { Component } from "react"; 
import { connect } from 'react-redux';
import { Link } from 'react-router';

class Header extends Component{

    render(){
        return(
        <div>
        <nav class="nav nav-pills flex-column flex-sm-row">
        <a class="flex-sm-fill text-sm-center nav-link active" href="#">Active</a>
        <a class="flex-sm-fill text-sm-center nav-link" href="#">Link</a>
        <a class="flex-sm-fill text-sm-center nav-link" href="#">Link</a>
        <a class="flex-sm-fill text-sm-center nav-link disabled" href="#">Disabled</a>
        </nav>
        </div>
        );
    }
}
export default Header;