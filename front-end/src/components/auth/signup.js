import React, { Component } from "react";
import { connect} from "react-redux"; 
import { reduxForm, submit, Field } from 'redux-form';
import * as actions from '../../actions';

const renderField = ({input, label, type, meta:{touched, error}}) =>(
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} />
            <br />
            <span className="alert"> {touched && error && <span>{error}</span>}</span>
        </div>
    </div>
)

class Signup extends Component{
    handleFormSubmit(formProps){
        //Call action create to sign up user
        this.props.signinUser(formProps);
    }
    renderAlert(){
        if(this.props.errorMessage){
            return(
                <div className="alert alert-danger"> {this.props.errorMessage}</div>
            )
        }
    }
    render(){
        const {error, handleSubmit, pristine, reset, submitting } = this.props;
        return(
            <div>
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                    <div>
                        <Field required name="username" type = "text" component ={renderField} label="Username" />
                        <Field required name= "userpass" type = "text" component ={renderField} label="Password" />
                        <Field required name= "userpassconfirm" type = "text" component ={renderField} label="PasswordConfirm" />
                        <br />
                        {error && <strong>{error}</strong>}
                    </div>

                {this.renderAlert()}
                <button action= "submit" className="btn btn-primary">Sign Up </button>
                </form>
            </div>
        );
    }
}
function validate(formProps){
    const errors={};
    if(formProps.password != formProps.passwordConfirm){
        errors.password = 'Passwords must match';
    }
    if(!formProps.username){
        errors.username = 'Please enter an username';
    }
    if(!formProps.password){
        errors.password = 'Please enter a password';
    }
    if(!formProps.passwordConfirm){
        errors.passwordConfirm = 'Please enter a password confirmation';
    }
    
    return errors;
}
Signup = connect(
    mapStateToProps,
    actions
)(Signup);
function mapStateToProps(state){
    return {errorMessage: state.auth.error};
}
export default reduxForm({
    form: 'signup',
    fields:['username', 'password', 'passwordConfirm'],
    validate
})(Signup);