import React, { Component } from "react";
import { connect} from "react-redux"; 
import {Field, reduxForm, submit } from 'redux-form';
import * as actions from '../../actions';

const renderField = ({input, label, type, meta:{touched, error}}) =>(
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} />
            <br />
            <span className="alert"> {touched && error && <span>{error}</span>}</span>
        </div>
    </div>
)
class Signin extends Component{

    handleFormSubmit({username,userpass}){
       console.log('info',username,userpass);
       this.props.signinUser({username,userpass});
    }
    render(){
        const {error, handleSubmit, pristine, reset, submitting } = this.props;

        return(
            <div>
        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <div>
            <Field required name ="username" type = "text" component={renderField} lable="Username" />
            <Field required name ="userpass" type = "text" component={renderField} lable="Password" />
            <br />
            {error && <strong>{error}</strong>}
            </div>
            <div>
                <button type ="submit" disabled={pristine || submitting}> Log In</button>
            </div>
        </form>
        
        </div>
    )
    }
}
function mapStateToProps(state){
    console.log('<<<<<<<<--------State----->>>>>>',state);
    return { errorMessage: state.auth.error};
}
Signin = connect(
    mapStateToProps,
    actions
)(Signin);
export default reduxForm({
    form: 'Signin'
})(Signin);