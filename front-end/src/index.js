import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reactThunk from 'redux-thunk';
import './index.css';
import App from './App';
import rootReducer from './reducers';
import { routerMiddleware, connectRouter, ConnectedRouter } from 'connected-react-router';
import 'bootstrap/dist/css/bootstrap.min.css';
import { AUTH_USER, UNAUTH_USER } from './actions/types';
import {createBrowserHistory} from 'history';

const history = createBrowserHistory();

const store = createStore(
    connectRouter(history)(rootReducer),
    compose(
        applyMiddleware(
            routerMiddleware(history),
            reactThunk
        )
    )
)
const token = localStorage.getItem('token');
if(token){
    store.dispatch({type: AUTH_USER})
}else{
    store.dispatch({type: UNAUTH_USER})
}

ReactDOM.render( 
<Provider store={store}> 
    <ConnectedRouter history={history}>

    <App /> 

    </ConnectedRouter>
</Provider>
, document.getElementById('container'));