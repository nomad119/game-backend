import React, { Component } from 'react';
import './App.css';
import Navigation from './components/Navigation';
import { Route, Link, Redirect} from 'react-router';
import { connect } from 'react-redux';
import Home from './containers/Home';
import Students from './containers/Students';
import Classes from './containers/Classes';
import Modules from './containers/Modules';
import * as actions from './actions';
import PublicLanding from './containers/PublicLanding';
import signin from './components/auth/signin';
import signup from './components/auth/signup';

class App extends Component {
  render() {
    return (

      <div className="App">
          <Route exact path= '/' render={ () =>( 
          this.props.isAuth ? (
            <Redirect to="/dashboard"/>
          ): (
            <Redirect to="/signup" />
          )  
        )} />

        <Route path= '/dashboard' component={Home} />

        <Route path="/signin" component={signin} />
        
        <Route path="/signup" component={signup} />
      </div>

    );
  }
}
function mapStateToProps(state){
  console.log(state.auth.authenticated);
  return {isAuth: state.auth.authenticated}
}
export default connect(mapStateToProps, actions)(App);
