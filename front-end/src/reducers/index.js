import { combineReducers } from 'redux';
import auth_reducer from './auth_reducer';
import message_reducer from './message_reducer';
import { reducer as formReducer } from 'redux-form';
import student_reducer from './student_reducer';

const rootReducer = combineReducers({
    form: formReducer,
    auth: auth_reducer,
    mess: message_reducer,
    students: student_reducer

});

export default rootReducer;