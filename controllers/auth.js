var config = require('../config');
var jwt = require('jwt-simple');

function tokenForUser(userID) {
    console.log('user callback', userID);
    var timestamp = new Date().getTime();
    return jwt.encode({
        sub: userID,
        iat: timestamp
    }, config.secret);
}
exports.signup = function (req, res, next) {
    var username = req.body.username;
    var userpass = req.body.userpass;
    if (!username || !userpass) {
        return res.status(422).send({
            error: 'You must enter both username and password'
        });
    }

};
exports.signin = function (req, res, next) {
    console.log("SEND IT",req.user);
    res.send({
        token: tokenForUser(req.user)
    })
};