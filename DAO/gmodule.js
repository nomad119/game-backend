var config = require('../config');
var mysql = require('mysql');
var pool = mysql.createPool({
    host: config.mysqlHost,
    user: config.mysqlUsername,
    password: config.mysqlPassword,
    database: config.mysqlDatabase
});
    //mysql querys for the modules
var insertModule = "INSERT INTO `gmodule`(score,mName,catID,sConstraint,studID)Values(?,?,?,?,?)";
var getModsById = "SELECT * FROM `gmodule` WHERE modID =?";
var getAllMods = "SELECT * FROM `gmodule`";
var deleteModule = "DELETE FROM `gmodule` WHERE modID = ?";
var updateModule = "UPDATE `gmodule`  SET score = ?, mName= ?, catID=?, sConstraint =?, studID =? WHERE modID=?";

// Insert module
exports.insertModule = function(modScore,modmName,modcatID,modSConstraint,modStudID){
    pool.query(
    insertModule, [modScore, modmName, modcatID, modSConstraint, modStudID],
    function (error, results, fields) {
        if (error) throw error;
        console.log('fromDB:',results);
        console.log({
            'mName': modmName,
            'catID': modcatID,
            'sConstraint':modSConstraint,
            'score': modScore,
            'studID': modStudID,
            'insertId': results.insertId
        });
        return({
            'mName': modmName,
            'catID': modcatID,
            'sConstraint':modSConstraint,
            'score': modScore,
            'studID': modStudID,
           
        })
    });
}
exports.getAllModules = function(){
//get all modules
pool.query(getAllMods, function (error, results, fields) {
    if (error) throw error;
    console.log(results);
    return(results);

});
}